﻿using ecommerce.Entity;
using ecommerce.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ecommerce.Controllers
{
    public class HomeController : Controller
    {
        DataContext db = new DataContext();

        // GET: Home

        // Stokta olan ürünleri onaylı ve anasayfada çıkacaksa gösterir.
        public ActionResult Index()
        {
            var products = db.Products.Where(i => i.IsApproved == true && i.IsHome == true && i.InStock == true)
                                      .Select(i=> new ProductModel()
                                      {
                                           BDescription = i.BDescription,
                                           CategoryId =i.CategoryId,
                                           From = i.From,
                                           BName = i.BName,
                                           Img = i.Img,
                                           Name = i.Name,
                                           Price = i.Price,
                                           Homeland = i.Homeland,
                                           Description = i.Description, 
                                           Id = i.Id
                                      }).ToList();

            return View(products);
        }

        // Hepsini Listele
        public ActionResult List(int? id)
        {
            var products = db.Products.Where(i => i.InStock == true)
                                      .Select(i => new ProductModel()
                                      {
                                          BDescription = i.BDescription,
                                          CategoryId = i.CategoryId,
                                          From = i.From,
                                          BName = i.BName,
                                          Img = i.Img,
                                          Name = i.Name,
                                          Price = i.Price,
                                          Homeland = i.Homeland,
                                          Description = i.Description,
                                          Id = i.Id
                                      }).AsQueryable();

            products = products.Where(i => i.CategoryId == id);

            return View(products);
        }

        public ActionResult Details(int id)
        {
            return View(db.Products.Where(i=> i.Id == id).FirstOrDefault());
        }

        public ActionResult AnneVeCocuk()
        {
            return View();
        }

        public ActionResult Iletisim()
        {
            return View();
        }

        public ActionResult Rss()
        {
            return View();
        }

        public ActionResult Teslimat()
        {
            return View();
        }

        public ActionResult Categories()
        {
            var categories = db.Categories.ToList();

            return View(categories);
        }

    }
}