﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ecommerce.Entity;
using ecommerce.Models;

namespace ecommerce.Controllers
{
    public class CartController : Controller
    {
        private DataContext db =new DataContext();

        // GET: Cart
        public ActionResult Index()
        {
            return View(GetCart());
        }

        public ActionResult AddToCart(int Id)
        {
            var product = db.Products.FirstOrDefault(i => i.Id == Id);
            if (product!=null)
            {
                GetCart().AddItem(product,1);
            }
            return RedirectToAction("Index");
        }

        public ActionResult RemoveToCart(int Id)
        {
            var product = db.Products.FirstOrDefault(i => i.Id == Id);
            if (product != null)
            {
                GetCart().DeleteItem(product);
            }
            return RedirectToAction("Index");
        }

        public Cart GetCart()
        {
            var cart = (Cart) Session["Cart"];
            if (cart == null)
            {
                cart = new Cart();
                Session["Cart"] = cart;
            }
            return cart;
        }


        
        public ActionResult Checkout(ShippingDetails entity)
        {
            var cart = GetCart();
            if (cart.CartLines.Count==0)
            {
                ModelState.AddModelError("UrunYokError","Sepetinizde Ürün Yok");
            }

            if (ModelState.IsValid)
            {
                SaveOrder(cart, entity);
                cart.Clear();
                return View("Completed");
            }
            else
            {
                return View(entity);
            }
            
        }

        private void SaveOrder(Cart cart, ShippingDetails entity)
        {
            var order = new Order();

            order.OrderNumber = (new Random()).Next(111111, 999999).ToString();
            order.Total = cart.Total();
            order.OrderDate = DateTime.Now;
            order.OrderState = EnumOrderState.Beklemede;
            order.Username = User.Identity.Name;
            order.Username = entity.Username;
            order.AdresBasligi = entity.AdresBasligi;
            order.Adres = entity.Adres;
            order.Sehir = entity.Sehir;
            order.Semt = entity.Semt;
            order.Mahalle = entity.Mahalle;
            order.PostaKodu = entity.PostaKodu;


            order.OrderLines = new List<OrderLine>();
            foreach (var pr in cart.CartLines)
            {
                var  ol = new OrderLine();
                ol.Quantity = pr.Quantity;
                ol.Price = pr.Quantity * pr.Product.Price;
                ol.ProductId = pr.Product.Id;

                order.OrderLines.Add(ol);
            }

            db.Orders.Add(order);
            db.SaveChanges();

        }
    }
}