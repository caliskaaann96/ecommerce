﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ecommerce.Models
{
    public class Register
    {
        [Required]
        [DisplayName("Adınız")]
        public string Name { get; set; }

        [Required]
        [DisplayName("Kullanıcı Adınız")]
        public string UserName { get; set; }

        [Required]
        [DisplayName("Emailiniz")]
        [EmailAddress(ErrorMessage ="Geçerli Bir Eposta Adresi Giriniz.")]
        public string Email { get; set; }

        [Required]
        [DisplayName("Sifreniz")]
        public string Password { get; set; }

        [Required]
        [DisplayName("Tekrar Sifreniz")]
        [Compare("Password",ErrorMessage ="Sifreler Uyuşmuyor.")]
        public string RePassword { get; set; }
    }
}