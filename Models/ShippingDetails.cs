﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ecommerce.Models
{
    public class ShippingDetails
    {
        
        public string Username { get; set; }
        [Required(ErrorMessage = "Adres Başlıgını Giriniz")]
        public string AdresBasligi { get; set; }
        [Required(ErrorMessage = "Adres Giriniz")]
        public string Adres { get; set; }
        [Required(ErrorMessage = "Şehir Giriniz")]
        public string Sehir { get; set; }
        [Required(ErrorMessage = "Semt Giriniz")]
        public string Semt { get; set; }
        [Required(ErrorMessage = "Mahalle Giriniz")]
        public string Mahalle { get; set; }
        [Required(ErrorMessage = "Posta Kodu Giriniz")]
        public string  PostaKodu { get; set; }
    }
}