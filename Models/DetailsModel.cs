﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecommerce.Models
{
    public class DetailsModel
    {
        public ProductModel ProductModel  { get; set; }
        public List<ProductModel> Products { get; set; }
    }
}