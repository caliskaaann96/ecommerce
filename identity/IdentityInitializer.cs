﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ecommerce.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ecommerce.identity
{
    public class IdentityInitializer : CreateDatabaseIfNotExists<IdentityDataContext>
    {
        protected override void Seed(IdentityDataContext context)
        {
            //Roller
            if (!context.Roles.Any(i=> i.Name == "admin"))
            {
                var store = new RoleStore<ApplicationRole>(context);
                var managar =new RoleManager<ApplicationRole>(store);

                var role = new ApplicationRole(){Name = "admin",Description = "admin rolü"};

                managar.Create(role);
            }

            if (!context.Roles.Any(i => i.Name == "user"))
            {
                var store = new RoleStore<ApplicationRole>(context);
                var managar = new RoleManager<ApplicationRole>(store);

                var role = new ApplicationRole() { Name = "user", Description = "user rolü" }; 

                managar.Create(role);
            }

            if (!context.Roles.Any(i => i.Name == "user"))
            {
                var store = new RoleStore<ApplicationRole>(context);
                var managar = new RoleManager<ApplicationRole>(store);

                var role = new ApplicationRole() { Name = "user", Description = "user rolü" };

                managar.Create(role);
            }

            if (!context.Roles.Any(i => i.Name == "erkancaliskan"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var managar = new UserManager<ApplicationUser>(store);

                var user = new ApplicationUser(){Name = "erkan",UserName = "erkancaliskan",Email = "caliskan.erkaann@gmail.com"};

                managar.Create(user,"1234567");
                managar.AddToRole(user.Id, "admin");
                managar.AddToRole(user.Id, "user");
            }
            if (!context.Roles.Any(i => i.Name == "asircaliskan"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var managar = new UserManager<ApplicationUser>(store);

                var user = new ApplicationUser() { Name = "aşır", UserName = "asircaliskan", Email = "caliskan.asirr@gmail.com" };

                managar.Create(user, "1234567");
                managar.AddToRole(user.Id, "user");
            }

            base.Seed(context); 
        }
    }
}