﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecommerce.Entity
{
    public class Product
    {
        public int Id { get; set; }
        public string Name  { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public string Img { get; set; }
        public bool IsApproved { get; set; }
        public int CategoryId { get; set; }
        public string From { get; set; }
        public bool IsHome { get; set; }
        public string Homeland { get; set; }
        public string BDescription { get; set; }
        public string BName { get; set; }
        public bool InStock { get; set; }

        public Category Category { get; set; }
    }
}